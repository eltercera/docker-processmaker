#!/bin/bash

# Apache PHP default values
export PHP_APACHE_INI="/etc/php/7.1/apache2/php.ini"
export PHP_CLI_INI="/etc/php/7.1/cli/php.ini"

export PHP_MEMORY_LIMIT=${PHP_MEMORY_LIMIT:-"512M"}

# Uploading Large Input Documents
# http://wiki.processmaker.com/3.0/Additional_Configuration#Uploading_Large_Input_Documents
export PHP_POST_MAX_SIZE=${PHP_POST_MAX_SIZE:-"24M"}
export PHP_UPLOAD_MAX_FILESIZE=${PHP_UPLOAD_MAX_FILESIZE:-"24M"}

# Session
# see http://wiki.processmaker.com/3.0/Additional_Configuration#Managing_Login_Sessions_in_ProcessMaker
export PHP_SESSION_GC_MAXLIFETIME=${PHP_SESSION_GC_MAXLIFETIME:-"1440"}
export PHP_SESSION_CACHE_EXPIRE=${PHP_SESSION_CACHE_EXPIRE:-"180"}
export PHP_SESSION_CACHE_LIMITER=${PHP_SESSION_CACHE_LIMITER:-"nocache"}

# http://wiki.processmaker.com/3.0/Additional_Configuration#More_Rows_in_an_Output_Document
export PHP_MAX_INPUT_VARS=${PHP_MAX_INPUT_VARS:-"8000"}

export PROCESSMAKER_SCRIPT="/opt/processmaker/processmaker"


function deploy_php_config {
    # Set apache php.ini
    sed -i "s/^memory_limit.*=.*/memory_limit = $PHP_MEMORY_LIMIT/" $PHP_APACHE_INI \
        && sed -i "s/^short_open_tag.*=.*/short_open_tag = on/" $PHP_APACHE_INI \
        && sed -i "s/^post_max_size.*=.*/post_max_size = $PHP_POST_MAX_SIZE/" $PHP_APACHE_INI \
        && sed -i "s/^upload_max_filesize.*=.*/upload_max_filesize = $PHP_UPLOAD_MAX_FILESIZE/" $PHP_APACHE_INI \
        && sed -i "s/^session\.gc_maxlifetime.*=.*/session.gc_maxlifetime = $PHP_SESSION_GC_MAXLIFETIME/" $PHP_APACHE_INI \
        && sed -i "s/^session\.cache_limiter.*=.*/session.cache_limiter = $PHP_SESSION_CACHE_LIMITER/" $PHP_APACHE_INI \
        && sed -i "s/^session\.cache_expire.*=.*/session.cache_expire = $PHP_SESSION_CACHE_EXPIRE/" $PHP_APACHE_INI \
        && sed -i "s/; max_input_vars.*=.*/max_input_vars = $PHP_MAX_INPUT_VARS/" $PHP_APACHE_INI \
        || return 1
    # Set cli php.ini
    sed -i "s/^short_open_tag.*=.*/short_open_tag = on/" $PHP_CLI_INI \
        && sed -i "s/^post_max_size.*=.*/post_max_size = $PHP_POST_MAX_SIZE/" $PHP_CLI_INI \
        && sed -i "s/^upload_max_filesize.*=.*/upload_max_filesize = $PHP_UPLOAD_MAX_FILESIZE/" $PHP_CLI_INI \
        && sed -i "s/^session\.gc_maxlifetime.*=.*/session.gc_maxlifetime = $PHP_SESSION_GC_MAXLIFETIME/" $PHP_CLI_INI \
        && sed -i "s/^session\.cache_limiter.*=.*/session.cache_limiter = $PHP_SESSION_CACHE_LIMITER/" $PHP_CLI_INI \
        && sed -i "s/^session\.cache_expire.*=.*/session.cache_expire = $PHP_SESSION_CACHE_EXPIRE/" $PHP_CLI_INI \
        && sed -i "s/; max_input_vars.*=.*/max_input_vars = $PHP_MAX_INPUT_VARS/" $PHP_CLI_INI \
        || return 1

    if [ ! -z $PHP_DATE_TIMEZONE ]; then
      time_zone_aux=${PHP_DATE_TIMEZONE//\//\\/}
      sed -i "s/^date\.timezone.*=.*/date.timezone = $time_zone_aux/" $PHP_APACHE_INI
      sed -i "s/^date\.timezone.*=.*/date.timezone = $time_zone_aux/" $PHP_CLI_INI
    fi
}


deploy_php_config || exit 1

if [ ! -f $PROCESSMAKER_SCRIPT ]; then
    [[ -f $PROCESSMAKER_TARGZ ]] || exit 1
    tar -C /opt/ -xzf $PROCESSMAKER_TARGZ || exit 1
fi

if [ -f /var/run/apache2/apache2.pid ]; then
    rm -f /var/run/apache2/apache2.pid
fi

if [ "$1" == "processmaker" ]; then
	shift
	php $PROCESSMAKER_SCRIPT $@
else
	apache2ctl -DFOREGROUND $@
fi
