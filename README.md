# Process Maker CE

Deploy [Process Maker CE](http://wiki.processmaker.com/) on Apache 2.4, PHP 5.6. and external Database

# Supported tags and respective `Dockerfile` links

- [`3.2`, `3.2.3`, `3.2.3-jessie-apache`, `latest`, (*debian/jessie/3.2.3/Dockerfile*)](https://bitbucket.org/eltercera/docker-processmaker/src/master/debian/jessie/3.2.3/Dockerfile)
- [`3.2.2`, `3.2.2-jessie-apache`, (*debian/jessie/3.2.2/Dockerfile*)](https://bitbucket.org/eltercera/docker-processmaker/src/master/debian/jessie/3.2.2/Dockerfile)
- [`3.2.1`, `3.2.1-jessie-apache`, (*debian/jessie/3.2.1/Dockerfile*)](https://bitbucket.org/eltercera/docker-processmaker/src/master/debian/jessie/3.2.1/Dockerfile)

# How to use this image

```
docker run -d \
    --link some-mysql:mysql \
    -p 8080:80 \
    --name some-pkm \
    eltercera/docker-processmaker
```
# Configuration

* Go to  http://hostip:8080/  and [Setting up ProcessMaker](http://wiki.processmaker.com/3.0/ProcessMaker_Generic_Installation#Step_4:_Installation_Wizard)

## Environment variables
All variables are optionals.

### Apache

* *RUN_USER:* (Default: "apache") User name to run run apache
* *RUN_GROUP:* (Default: "apache") User group to root apache
* *RUN_UID:* (Default: $(id -u $RUN_USER))
* *RUN_GID:* (Default: $(id -u $RUN_GROUP))
* *HTTP_PORT:* (Default: 80)
* *HTTPD_SERVER_ADMIN* To setting ServerAdmin en httpd.conf

### PHP

* *PHP_MEMORY_LIMIT:* (Default: 256M)
* *PHP_DATE_TIMEZONE:* To set Date time zone

#### [Uploading Large Input Documents](http://wiki.processmaker.com/3.0/Additional_Configuration#Uploading_Large_Input_Documents)
* *PHP_POST_MAX_SIZE:* (Default: 24M)
* *PHP_UPLOAD_MAX_FILESIZE:* (Default: 24M)

#### [Session](http://wiki.processmaker.com/3.0/Additional_Configuration#Managing_Login_Sessions_in_ProcessMaker)
* *PHP_SESSION_GC_MAXLIFETIME:* (Default: 1440)
* *PHP_SESSION_CACHE_EXPIRE:* (Default: 180)
* *PHP_SESSION_CACHE_LIMITER:* (Default: nocache)

#### [More Rows in an Output Document](http://wiki.processmaker.com/3.0/Additional_Configuration#More_Rows_in_an_Output_Document)
* *PHP_MAX_INPUT_VARS:* (Default: 8000)

#### [Insertion of quotation marks and backslashes](http://wiki.processmaker.com/3.0/Additional_Configuration#Insertion_of_quotation_marks_and_backslashes)
* *PHP_MAGIC_QUOTES_GPC:* (Default: off)
* *PHP_MAGIC_QUOTES_RUNTIME:* (Default: off)
* *PHP_MAGIC_QUOTES_SYBASE:* (Default: off)

# Persistent data

To keep the persistent data of a processmaker instance, it is enough to generate volumes in the directories:

 - `/opt/pm/processmaker/shared`
 - `/opt/pm/processmaker/workflow/engine/config`
 - `opt/pm/plugins` (Optional).

```
docker run -d \
    --link some-mysql:mysql \
    -p 8080:8080 \
    -e HTTP_PORT=8080 \
    --name some-pkm \
    -v some/path/shared:/opt/pm/processmaker/shared \
    -v some/path/config:/opt/pm/processmaker/workflow/engine/config \
    eltercera/docker-processmaker
```