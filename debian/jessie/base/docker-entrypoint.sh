#!/bin/bash
set -ex

# Users to run
export RUN_USER=${RUN_USER:-"www-data"}
export RUN_GROUP=${RUN_GROUP:-"www-data"}
export HTTP_PORT=${HTTP_PORT:-"80"}
export RUN_UID=${RUN_UID:-$(id -u $RUN_USER)}
export RUN_GID=${RUN_GID:-$(id -u $RUN_GROUP)}
export APACHE_RUN_USER=$RUN_USER
export APACHE_RUN_GROUP=$RUN_GROUP

# Apache PHP default values
export APACHE_DEFAULT_SITE_CONF="/etc/apache2/sites-available/000-default.conf"
export PHP_APACHE_INI="/etc/php5/apache2/php.ini"
export PHP_CLI_INI="/etc/php5/cli/php.ini"
export PHP_MODS_AVAILABLE="/etc/php5/mods-available"

export PHP_MEMORY_LIMIT=${PHP_MEMORY_LIMIT:-"256M"}

# Uploading Large Input Documents
# http://wiki.processmaker.com/3.0/Additional_Configuration#Uploading_Large_Input_Documents
export PHP_POST_MAX_SIZE=${PHP_POST_MAX_SIZE:-"24M"}
export PHP_UPLOAD_MAX_FILESIZE=${PHP_UPLOAD_MAX_FILESIZE:-"24M"}

# Session
# see http://wiki.processmaker.com/3.0/Additional_Configuration#Managing_Login_Sessions_in_ProcessMaker
export PHP_SESSION_GC_MAXLIFETIME=${PHP_SESSION_GC_MAXLIFETIME:-"1440"}
export PHP_SESSION_CACHE_EXPIRE=${PHP_SESSION_CACHE_EXPIRE:-"180"}
export PHP_SESSION_CACHE_LIMITER=${PHP_SESSION_CACHE_LIMITER:-"nocache"}

# http://wiki.processmaker.com/3.0/Additional_Configuration#More_Rows_in_an_Output_Document
export PHP_MAX_INPUT_VARS=${PHP_MAX_INPUT_VARS:-"8000"}

# http://wiki.processmaker.com/3.0/Additional_Configuration#Insertion_of_quotation_marks_and_backslashes
export PHP_MAGIC_QUOTES_GPC=${PHP_MAGIC_QUOTES_GPC:-"off"}
export PHP_MAGIC_QUOTES_RUNTIME=${PHP_MAGIC_QUOTES_RUNTIME:-"off"}
export PHP_MAGIC_QUOTES_SYBASE=${PHP_MAGIC_QUOTES_SYBASE:-"off"}


# PM default values
export PM_DIR="/opt/pm"
export PM_ROOTDIR="$PM_DIR/processmaker"
export PM_SCRIPT="$PM_ROOTDIR/processmaker"
export PM_SHAREDDIR="$PM_ROOTDIR/shared"
export PM_PLUGINS="$PM_DIR/plugins"


function deploy_php_config {
    # Set apache php.ini
    sed -i "s/^memory_limit.*=.*/memory_limit = $PHP_MEMORY_LIMIT/" $PHP_APACHE_INI \
        && sed -i "s/^short_open_tag.*=.*/short_open_tag = on/" $PHP_APACHE_INI \
        && sed -i "s/^post_max_size.*=.*/post_max_size = $PHP_POST_MAX_SIZE/" $PHP_APACHE_INI \
        && sed -i "s/^upload_max_filesize.*=.*/upload_max_filesize = $PHP_UPLOAD_MAX_FILESIZE/" $PHP_APACHE_INI \
        && sed -i "s/^session\.gc_maxlifetime.*=.*/session.gc_maxlifetime = $PHP_SESSION_GC_MAXLIFETIME/" $PHP_APACHE_INI \
        && sed -i "s/^session\.cache_limiter.*=.*/session.cache_limiter = $PHP_SESSION_CACHE_LIMITER/" $PHP_APACHE_INI \
        && sed -i "s/^session\.cache_expire.*=.*/session.cache_expire = $PHP_SESSION_CACHE_EXPIRE/" $PHP_APACHE_INI \
        && sed -i "s/; max_input_vars.*=.*/max_input_vars = $PHP_MAX_INPUT_VARS/" $PHP_APACHE_INI \
        || return 1
    # Set cli php.ini
    sed -i "s/^short_open_tag.*=.*/short_open_tag = on/" $PHP_CLI_INI \
        && sed -i "s/^post_max_size.*=.*/post_max_size = $PHP_POST_MAX_SIZE/" $PHP_CLI_INI \
        && sed -i "s/^upload_max_filesize.*=.*/upload_max_filesize = $PHP_UPLOAD_MAX_FILESIZE/" $PHP_CLI_INI \
        && sed -i "s/^session\.gc_maxlifetime.*=.*/session.gc_maxlifetime = $PHP_SESSION_GC_MAXLIFETIME/" $PHP_CLI_INI \
        && sed -i "s/^session\.cache_limiter.*=.*/session.cache_limiter = $PHP_SESSION_CACHE_LIMITER/" $PHP_CLI_INI \
        && sed -i "s/^session\.cache_expire.*=.*/session.cache_expire = $PHP_SESSION_CACHE_EXPIRE/" $PHP_CLI_INI \
        && sed -i "s/; max_input_vars.*=.*/max_input_vars = $PHP_MAX_INPUT_VARS/" $PHP_CLI_INI \
        || return 1

    if [ ! -f $PHP_MODS_AVAILABLE/magic_quotes.ini ]; then
    	echo "
magic_quotes_gpc = $PHP_MAGIC_QUOTES_GPC
magic_quotes_runtime = $PHP_MAGIC_QUOTES_RUNTIME
magic_quotes_sybase = $PHP_MAGIC_QUOTES_SYBASE" > $PHP_MODS_AVAILABLE/magic_quotes.ini || return 1
    else
        sed -i "s/^magic_quotes_gpc.*=.*/magic_quotes_gpc = $PHP_MAGIC_QUOTES_GPC/" $PHP_MODS_AVAILABLE/magic_quotes.ini \
            && sed -i "s/^magic_quotes_runtime.*=.*/magic_quotes_runtime = $PHP_MAGIC_QUOTES_RUNTIME/" $PHP_MODS_AVAILABLE/magic_quotes.ini \
            && sed -i "s/^magic_quotes_sybase.*=.*/magic_quotes_sybase = $PHP_MAGIC_QUOTES_SYBASE/" $PHP_MODS_AVAILABLE/magic_quotes.ini \
            || return 1
    fi
    php5enmod magic_quotes

    if [ ! -z $PHP_DATE_TIMEZONE ]; then
        if [ -f /usr/share/zoneinfo/$PHP_DATE_TIMEZONE ]; then
          cp /usr/share/zoneinfo/$PHP_DATE_TIMEZONE /etc/localtime
          echo $PHP_DATE_TIMEZONE >  /etc/timezone
          time_zone_aux=${PHP_DATE_TIMEZONE//\//\\/}
          sed -i "s/^date\.timezone.*=.*/date.timezone = $time_zone_aux/" $PHP_APACHE_INI
          sed -i "s/^date\.timezone.*=.*/date.timezone = $time_zone_aux/" $PHP_CLI_INI
        fi
    fi
}

function deploy_apache_config() {
    if [ -z $(id -u $RUN_GROUP) ]; then
    	if [ -z "$RUN_GID" ]; then
            addgroup --system $RUN_GROUP || return 1
    	else
            addgroup --system --gid $RUN_GID $RUN_GROUP || return 1
    	fi
    fi

    if [ -z $(id -u $RUN_USER) ]; then
    	if [ -z "$RUN_UID" ]; then
            adduser --system --home $PM_ROOTDIR --no-create-home --gid $RUN_GID $RUN_USER || return 1
    	else
            adduser --system --home $PM_ROOTDIR --no-create-home --uid $RUN_UID --gid $RUN_GID $RUN_USER || return 1
    	fi
    fi

    sed -i "s/^export APACHE_RUN_USER=.*/export APACHE_RUN_USER=$APACHE_RUN_USER/" /etc/apache2/envvars
    sed -i "s/^export APACHE_RUN_GROUP=.*/export APACHE_RUN_GROUP=$APACHE_RUN_GROUP/" /etc/apache2/envvars

    [[ -z $HTTPD_SERVER_ADMIN ]] || sed -i "s/^ServerAdmin.*/ServerAdmin $HTTPD_SERVER_ADMIN/" $APACHE_DEFAULT_SITE_CONF

	sed -i "s/^Listen 80\$/Listen $HTTP_PORT/" /etc/apache2/ports.conf
	sed -i "s/^<VirtualHost.*/<VirtualHost \*:$HTTP_PORT>/" $APACHE_DEFAULT_SITE_CONF
    pm_dir=${PM_ROOTDIR//\//\\/}
    sed -i "s/^DocumentRoot.*/DocumentRoot $pm_dir\/workflow\/public_html/" $APACHE_DEFAULT_SITE_CONF \
        && sed -i "s/^<Directory.*/<Directory \"$pm_dir\/workflow\/public_html\">/" $APACHE_DEFAULT_SITE_CONF \
        || return 1
    ln -sfT /dev/stderr "/var/log/apache2/error.log"
    ln -sfT /dev/stdout "/var/log/apache2/access.log"
    a2enmod expires
    a2enmod rewrite
    a2enmod filter
    a2enmod deflate
    chown -R $RUN_USER:$RUN_GROUP $PM_DIR || return 1
}

function deploy_pm() {
    [[ -f /opt/processmaker-$PM_VERSION-community.tar.gz ]] || return 1
    su -s /bin/bash \
        -c "tar -C $PM_DIR -xzkf \"/opt/processmaker-$PM_VERSION-community.tar.gz\"" \
        $RUN_USER || return 0
}

function deploy_pm_plugins() {

    if [ -d $PM_PLUGINS ]; then
        find $PM_PLUGINS -maxdepth 1 -type d | grep -v ^$PM_PLUGINS$ | rev | cut -f1 -d '/' | rev | while read plugin_name
        do
            if [ ! -d  $PM_ROOTDIR/workflow/engine/plugins/$plugin_name ]; then
                ln -s $PM_PLUGINS/$plugin_name/$plugin_name $PM_ROOTDIR/workflow/engine/plugins/$plugin_name
            fi

            if [ ! -f  $PM_ROOTDIR/workflow/engine/plugins/$plugin_name.php ]; then
                ln -s $PM_PLUGINS/$plugin_name/$plugin_name.php $PM_ROOTDIR/workflow/engine/plugins/$plugin_name.php
            fi
        done
    fi
}

function deploy() {

    # Deploy only with root user.
    if [ $(id -u) -ne "0" ]; then
        echo "Deploy only with root user."
        return 1
    fi

    deploy_php_config \
        && deploy_apache_config \
        && deploy_pm || return 1

    return 0
}

if [ ! -f $PM_SCRIPT ]; then
    deploy || exit 1
fi

if [ -f /var/run/apache2/apache2.pid ]; then
    rm -f /var/run/apache2/apache2.pid
fi

deploy_pm_plugins
apache2ctl -DFOREGROUND $@
